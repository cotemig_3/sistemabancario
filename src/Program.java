import java.util.Scanner;

// Autores: João Victor  e Márcio

public class Program {


    private static void menu(){
         Banco banco1 = new Banco();
        int operacao =1, opcConta;
        Scanner ler = new Scanner(System.in);
        do{

            System.out.println("---------------------- ");
            System.out.println("|   Menu Principal   | ");
            System.out.println("----------------------");
            System.out.println("| 1 - Criar Conta    |");
            System.out.println("| 2 - Saldo          |");
            System.out.println("| 3 - Depósito       | ");
            System.out.println("| 4 - Saque          |");
            System.out.println("| 0 - Sair           |");
            System.out.println("---------------------- ");
            System.out.print("Operação ->  ");
            operacao = ler.nextInt();

            switch(operacao){
                case 1:
                System.out.println("Vc escolheu Criar conta ");
                System.out.println("Qual tipo de conta você deseja criar ?");

                System.out.println("1 - Poupanca");
                System.out.println("2 - Corrente");
                System.out.print("Opcao ->  ");

                opcConta = ler.nextInt();

                switch(opcConta){
                    case 1:
                        System.out.println("Criar conta poupanca");
                        System.out.println("Digite o número da conta :");
                        ler.nextLine();
                        String numeroConta = ler.nextLine();

                        Conta c1 = new Poupanca();
                        c1.setNumConta(numeroConta);
                        c1.setSaldo(0.00);
                        banco1.addConta(c1);

                        System.out.println("Conta criada com sucesso !!");

                        break;
                    case 2:
                        System.out.println("Criar conta poupanca");
                        System.out.println("Digite o número da conta :");
                        ler.nextLine();
                        String numeroConta2 = ler.nextLine();

                        Conta c2 = new Corrente();
                        c2.setNumConta(numeroConta2);
                        c2.setSaldo(0.00);
                        banco1.addConta(c2);

                        System.out.println("Conta criada com sucesso !!");

                        break;
                        default:
                        System.out.println("Opcao invalida !!");

                }

                break;
                case 2:
                System.out.println("Vc escolheu Saldo ");
                System.out.println("Para qual tipo de conta deseja visualizar o saldo ?");

                System.out.println("1 - Poupanca");
                System.out.println("2 - Corrente");
                System.out.print("Opcao ->  ");

                opcConta = ler.nextInt();

                switch(opcConta){
                    case 1 :
                        System.out.println("Voce escolheu conta poupanca");
                        System.out.println("Digite o numero da conta:");
                        System.out.print("Numero da conta ->  ");
                        ler.nextLine();
                        String numConta1 = ler.nextLine();
                        Conta conta1 = banco1.procurarConta("Poupanca",numConta1 );

                        if(conta1 != null){
                            System.out.println("Saldo Atual :"+conta1.getSaldo());
                        }else{
                            System.out.println("Conta não encontrada !!");
                        }
                        break;
                    case 2:
                        System.out.println("Voce escolheu conta Corrente");
                        System.out.println("Digite o numero da conta:");
                        System.out.print("Numero da conta ->  ");
                        ler.nextLine();
                        String numConta2 = ler.nextLine();
                        Conta conta2 = banco1.procurarConta("Corrente",numConta2 );

                        if(conta2 != null){
                            System.out.println("Saldo Atual :"+conta2.getSaldo());
                        }else{
                            System.out.println("Conta não encontrada !!");
                        }
                        break;

                        default:
                        System.out.println("Opcao invalida !!");
                }
                break;
                case 3:
                System.out.println("Vc escolheu Depósito ");
                    System.out.println("Para qual tipo de conta deseja realizar depósito ?");

                    System.out.println("1 - Poupanca");
                    System.out.println("2 - Corrente");

                    System.out.print("Opcao ->  ");

                    opcConta = ler.nextInt();

                    switch(opcConta){

                        case 1:
                            System.out.println("Voce escolheu conta poupanca");
                            System.out.println("Digite o numero da conta:");
                            System.out.print("Numero da conta ->  ");
                            ler.nextLine();
                            String numConta1 = ler.nextLine();
                            Conta conta1 = banco1.procurarConta("Poupanca",numConta1 );

                            if(conta1 != null){
                                System.out.println("Quanto deseja Depositar ?");
                                double valorDep = ler.nextDouble();
                                conta1.depositar(valorDep);
                                System.out.println("Depósito realizado com sucesso !!");
                                System.out.println("Saldo Atual :"+conta1.getSaldo());
                            }else{
                                System.out.println("Conta não encontrada !!");
                            }
                            break;
                        case 2:
                            System.out.println("Voce escolheu conta Corrente");
                            System.out.println("Digite o numero da conta:");
                            System.out.print("Numero da conta ->  ");
                            ler.nextLine();
                            String numConta2 = ler.nextLine();
                            Conta conta2 = banco1.procurarConta("Corrente",numConta2 );

                            if(conta2 != null){
                                System.out.println("Quanto deseja Depositar ?");
                                double valorDep = ler.nextDouble();
                                conta2.depositar(valorDep);
                                System.out.println("Depósito realizado com sucesso !!");
                                System.out.println("Saldo Atual :"+conta2.getSaldo());
                            }else{
                                System.out.println("Conta não encontrada !!");
                            }
                            break;
                        default:
                            System.out.println("Opcao Invalida !");

                    }


                break;
                case 4:
                System.out.println("Vc escolheu Saque ");

                    System.out.println("Para qual tipo de conta deseja realizar o saque ?");

                    System.out.println("1 - Poupanca");
                    System.out.println("2 - Corrente");

                    System.out.print("Opcao ->  ");

                    opcConta = ler.nextInt();

                    switch(opcConta){
                        case 1:
                            System.out.println("Voce escolheu conta poupanca");
                            System.out.println("Digite o numero da conta:");
                            System.out.print("Numero da conta ->  ");
                            ler.nextLine();
                            String numConta1 = ler.nextLine();
                            Conta conta1 = banco1.procurarConta("Poupanca",numConta1 );

                            if(conta1 != null){
                                System.out.println("Quanto deseja Sacar ?");
                                double valorSaq = ler.nextDouble();
                                if(conta1.sacar(valorSaq)){
                                    System.out.println("Saque realizado com sucesso !");
                                    System.out.println("Saldo Atual :"+conta1.getSaldo());


                                }else{
                                    System.out.println("Saque não realizado !");

                                }

                            }else{
                                System.out.println("Conta não encontrada !!");
                            }

                            break;
                        case 2:
                            System.out.println("Voce escolheu conta Corrente");
                            System.out.println("Digite o numero da conta:");
                            System.out.print("Numero da conta ->  ");
                            ler.nextLine();
                            String numConta2 = ler.nextLine();
                            Conta conta2 = banco1.procurarConta("Poupanca",numConta2 );

                            if(conta2 != null){
                                System.out.println("Quanto deseja Sacar ?");
                                double valorSaq = ler.nextDouble();
                                if(conta2.sacar(valorSaq)){
                                    System.out.println("Saque realizado com sucesso !");
                                    System.out.println("Saldo Atual :"+conta2.getSaldo());


                                }else{
                                    System.out.println("Saque não realizado !");

                                }

                            }else{
                                System.out.println("Conta não encontrada !!");
                            }
                            break;
                        default:
                            System.out.println("opcao invalida !");
                    }

                break;
                case 0:
                System.out.println("Aplicação finalizada com sucesso ");
                break;

                default:
                System.out.println("Opção inválida ");
                break;

            }


        }while(operacao !=0);
    }

    public static void main(String[] args) {
 
        System.out.println("Criando a estrutura de menu");
        System.out.println("Teste");
        menu();
    }
}
