public class Corrente extends Conta {
    @Override
    public void depositar(double valor) {
        setSaldo(getSaldo() + valor + valor * 0.05);
    }

    @Override
    public boolean sacar(double valor) {
        if(valor + 2.00 < getSaldo() || valor + 2.00 == getSaldo()){
            setSaldo(getSaldo() - valor - 2.00);
            return true;
        }
        return false;
    }

    @Override
    double consultarSaldo() {
        return getSaldo();
    }
}
