import java.util.ArrayList;

public class Banco {
    private String nome;
    private ArrayList<Conta> contas;

    public Banco(){
        contas = new ArrayList<>();
    }


    public void addConta(Conta conta){
        this.contas.add(conta);

    }

    public Conta procurarConta(String tipo, String numConta){
        Conta buscConta = null;
        for (Conta conta: contas) {
            if(conta.getNumConta().equals(numConta) && conta.getClass().getName().equals(tipo)){
                buscConta = conta;
            }


        }

        return buscConta;

    }

}
